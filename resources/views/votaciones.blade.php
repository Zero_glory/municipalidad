<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/app.js')}}"></script>
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Formulario de votación
                </div>

                <form method="post" action="{{ route('votaciones.store') }}">
                  <input type="hidden" name="user" value="{{Auth::id()}}"><br />

                    <div class="mb-3">
                        @csrf
                      <label class="form-label">Nombre y apellido</label>   <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Alias</label>   <input type="text" class="form-control" id="alias" name="alias">
                    </div>
                      <div class="mb-3">
                      <label class="form-label">Rut</label>   <input type="text" class="form-control" id="rut" name="rut">
                    </div>
                      <div class="mb-3">
                      <label class="form-label">Email</label><input type="text" class="form-control" id="email" name="email">
                    </div>
                      <div class="mb-3">
                      <label class="form-label">Región</label>
                      <select class="form-select" id="region" name="region" id="region-dropdown">
                        @foreach($regiones as $region)
                        <option value="{{$region->id}}">{{$region->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                      <div class="mb-3">
                      <label class="form-label">Comuna</label>
                      <select class="form-select" id="comuna" name="comuna" id="comuna-dropdown">
                        @foreach($comunas as $comuna)
                        <option value="{{$comuna->id}}">{{$comuna->nombre}}</option>
                        @endforeach
                      </select>
                    </div>
                      <div class="mb-3">
                      <label class="form-label">Candidato</label>

                      <div class="mb-3">
                        <select class="form-select" id="candidato_id" name="candidato_id">
                            @foreach($candidatos as $candidato)
                            <option value="{{$candidato->id}}">{{$candidato->nombre}}</option>
                            @endforeach
                          </select>
                    </div>
                    </div>
                    <div class="mb-3">
                      <div id="msg"></div>

                        <label class="form-label">Como se enteró de Nosotros</label><div class="form-check">
                            <input class="form-check-input" type="checkbox" value="web" id="miElementoCheckbox" name="checkbox[]">
                            <label class="form-check-label" for="webCheck">
                              Web
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="tv" id="miElementoCheckbox" name="checkbox[]">
                            <label class="form-check-label" for="tvCheck">
                              TV
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="redessociales" id="miElementoCheckbox" name="checkbox[]">
                            <label class="form-check-label" for="redessocialesCheck">
                              Redes Sociales
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="amigo" id="miElementoCheckbox" name="checkbox[]">
                            <label class="form-check-label" for="amigoCheck">
                              Amigo
                            </label>
                          </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Votar</button>
                  </form>
            </div>
        </div>
        <script type="text/javascript">
       
      </script>
    </body>
</html>
