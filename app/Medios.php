<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medios extends Model
{
    protected $table = 'medios';
    protected $fillable = ['user_id','nombre'];
}
