<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ComunaController extends Controller
{
    protected $table = 'comunas';

    public function regions()
	{
		 return $this->belongsTo('regions');
	}
}
