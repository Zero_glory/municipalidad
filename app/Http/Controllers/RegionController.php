<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegionController extends Controller
{
    protected $table = 'regions';

    public function comuna()
	{
		 return $this->hasMany('comuna');
	}
}
