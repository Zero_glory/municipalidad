<?php

namespace App\Http\Controllers;

use App\Candidato;
use App\Comuna;
use App\Medios;
use App\Region;
use App\Votacion;
use Illuminate\Http\Request;

class VotacionController extends Controller
{
    public function index(){}
    public function create()
    {
       $regiones = Region::all();
       $candidatos = Candidato::all();
       $comunas = Comuna::all();
       $data['regiones'] = Region::get(["nombre","id"]);
       return view('votaciones',['candidatos' => $candidatos,'regiones' => $regiones,'comunas' => $comunas,'data' => $data]);
    }
    public function store(Request $request){
        $input = $request->all();
        $checkboxs = $request->get('checkbox');
        $request->validate([
            'nombre' => 'required',
            'alias' => 'required|min:5|unique:votacions',
            'rut' => 'required|unique:votacions',
            'email' => 'required|string|email|max:255,regex:/(.*)@example\.com$/i'

        ]);
        Votacion::create($input);
        
        foreach($checkboxs as $key => $value)
        {
        Medios::create([
        'user_id' => auth()->id(),
        'nombre' => $value,
          ]);
        }     
        return redirect('home');
    }

    public function getRegion()
    {
        $data['regiones'] = Region::get(["nombre","id"]);
        return view('votaciones',$data);
    }
    public function getComuna(Request $request)
    {
        $data['comunas'] = Comuna::where("region_id",$request->get('region'))
                    ->get(["nombre","id"]);
        return response()->json($data);
    }

    public function show(){}
    public function update(){}
    public function destroy(){}
}
