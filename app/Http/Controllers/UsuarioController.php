<?php

namespace App\Http\Controllers;

use App\Candidato;
use App\Region;
use App\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function index(){}
    public function create()
    {
       $regiones = Region::all();
       $candidatos = Candidato::all();
       return view('votaciones',['candidatos' => $candidatos]);
    }
    public function store(Request $request){
        $input = $request->all();
        $request->validate([
            'nombre' => 'required',
            'alias' => 'required|min:5|unique:users',
            'rut' => 'required|unique:users',
            'email' => 'required|string|email|max:255,regex:/(.*)@example\.com$/i'
        ]);
        User::create($input);
    }
    public function show(){}
    public function update(){}
    public function destroy(){}
}
