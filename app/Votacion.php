<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votacion extends Model
{
    protected $table = 'votacions';
    protected $fillable = ['nombre','alias','rut','email','region','comuna','candidato_id','region_id'];
}
