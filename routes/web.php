<?php

use Illuminate\Support\Facades\Response;
use App\Comuna;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('/votaciones', 'VotacionController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('getcomuna','VotacionController@getComuna');

